# Technical Debts

These would need to be addressed, as per your needs, if this implementation
is adopted. It would be better if the team/developer who are involved in
the effort are Apache Spark experienced, preferrably with Python.

## Links
  - https://aws.amazon.com/blogs/big-data/best-practices-to-scale-apache-spark-jobs-and-partition-data-with-aws-glue/
  - [https://support.snowflake.net/s/article/How-To-Use-AWS-Glue-With-Snowflake](https://support.snowflake.net/s/article/How-To-Use-AWS-Glue-With-Snowflake)
  - https://medium.com/@joaopedro.pinheiro88/how-to-increase-data-load-speed-from-database-with-pyspark-3741cccdf928
  - https://docs.aws.amazon.com/glue/latest/dg/monitor-glue.html
  - https://aws.amazon.com/blogs/big-data/load-data-incrementally-and-optimized-parquet-writer-with-aws-glue/
  
## Things to understand

### Source database size
 The above demonstrated code would perform well in a database of smaller size, you would still need to fine tune using Glue parameters for larger tables.

### Loading frequency
 This solution is not meant for real-time, like minute-by-minute data replication. This was meant mainly for time span of 6 hours or more 

### Pricing
  For the demo I did, the table/database sizes were small. Executing these
 multiple times during the day was less than $2/day.
  
  Now this would vary based of your database/table size. Hence understanding and evaluating prices ahead of time would be better.

  The following components used in the solution and optional would lead to cost:
 - AWS Glue
 - NAT Gateway (optional), if running glue in a private subnet.
 - S3
 - Step functions
 - Secrets Manager
 - Cloud watch logs

 In some cases, it might be easier to change the loading strategy a little bit and still get the pricing under control.

 ## Source Database
 Be on the lookout for how much connection your source database allows and the effect of data load.

## Technical Debts
The following are some technical debts left to be implemented, if you prefer to adopt :
 - Source schema changes
 - On demand Refresh / Full data reload methodology
 - Alert & Notification, can be achieved by Lambda solution, refer to AWS solution registry for a base implementation
 - Parallel data loading (optional)
 - UI (optional)