## Deployment

Since this is a reference prototype implementation, I have defined base cloudformation template scripts based on easier environment. You would need
to update based of your cloud settings.

1) Instantiate the cloud stack defined in [cloudformation/datasync_template.yaml](cloudformation/datasync_template.yaml). This will create the resources as shown below. I have the stack named 'glue-dsync-base'

![](doc/images/glue-dsync-base.jpg)

2) Instantiate the cloud stack defined in [cloudformation/datasync_jobtemplate.yaml](cloudformation/datasync_jobtemplate.yaml). The template import values from the stack defined in step-1. The stack name is defined as a parameter.

![](doc/images/dsync-job-defn.jpg)

3) Execute the script [bin/step_1_1_push_code_artifacts_s3.sh](bin/step_1_1_push_code_artifacts_s3.sh). This downloads the JDBC drivers and uploads the code and these libraries to corresponding folders in the S3 code bucket. The script determines the appropriate resource like S3 bucket name by introspecting the cloud stack defined in step 1.

```shell
./bin/step_1_1_push_code_artifacts_s3.sh glue-dsync-base
```

once executed, you can validate if the artifacts are infact in the S3 bucket.

![](doc/images/s3_code.jpg)

End!!